import { Component } from "react";
import Router from "react-router";
import { createHistory } from "history";
import { Provider } from "react-redux";
import Routes from "./Routes";

import { createStore } from "redux";
const store = createStore((state, action) => {});
const history = createHistory();

export default class Root extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={history} routes={Routes} />
      </Provider>
    );
  }
}