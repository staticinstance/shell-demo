import { Component } from "react";
import {
  Grid,
  Row,
  Col,
  Navbar,
  Nav
} from "react-bootstrap";

export default class AppShell extends Component {
  render() {
    return <div>
      <Navbar static fixed>
        <Navbar.Brand>Shell 1.2</Navbar.Brand>
      </Navbar>
      <Grid fluid>
        {this.props.children}
      </Grid>
    </div>
  }
}
