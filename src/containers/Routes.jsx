import { Route } from "react-router";
import AppShell from "./AppShell";
import Index from "./Index";

export default (
  <Route component={AppShell}>
    <Route path="/" component={Index}/>
  </Route>
);
