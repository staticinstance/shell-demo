import ReactDOM from "react-dom";
import Root from "./containers/Root";

require("bootstrap/dist/css/bootstrap.min.css");

ReactDOM.render(
  <Root/>,
  document.getElementById("app")
);
